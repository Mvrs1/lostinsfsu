# LostInSFSU

[![CircleCI](https://circleci.com/gh/Mvrs/lostinsfsu/tree/master.svg?style=svg)](https://circleci.com/gh/Mvrs/lostinsfsu/tree/master)

> Goto resource for _SFSU_ students to search for places that's not just a _dorm_

<div align="center">
	<img src="./client/src/assets/images/SFLogo.png" width="128" alt="logo">
</div>

LostInSFSU is a goto for incoming freshman and transfer students to find housing near campus that's not just your dorms. Students will be able to gives reviews on the places they've stayed so the next student will find the proccess of searching simpler.

## Tech Stack💎

- React and Redux
- Web Server: Nginx
- SQL: Postgres
- Node.js
- SASS: Can't always use CSS in JS
- Knex.js: SQL Query Builder

## TODO

- [x] Implement Routes for the base functionalities
- [x] Server endpoints working
- [x] Local Database running: controllers and models
- [x] Client talks to the Local Database
- [x] User Authentication and Hashing works
- [x] Redux: actions and reducers
- [ ] Entinties resolved
- [ ] UI up and looking beautiful
- [ ] Mapping available for housing
- [ ] Google Maps on search
- [ ] Reverse proxy backend for production
- [ ] Production Database running
- [ ] Implement TypeScript
- [ ] Integrate React Hooks
- [ ] Admin can create Listings
- [ ] Students can save listings for reference
- [ ] MVP architecture: The use MobX perhaps
- [ ] Student/Admin profiles
- [ ] Messaging

## Acknowledgement

This goal with this project is to get myself more comfortable with React and more confident in building full stack web applications. To also note: My cloud hosting for production will be Digital Ocean, just timing my decision for hosting my database as the costs is much higher than hosting a domain. My Domain is online and secure but the current build thats up wont be easy on the eyes.

## Contributions

> ⚠️ Notice: I'm **NOT** accepting pull requests currently.

## License📚

LostInSFSU is under an Apache license. See the [LICENSE](LICENSE) for more information.
